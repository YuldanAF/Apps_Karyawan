﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Karyawan : System.Web.UI.Page
{
    string connectionString = @"Data Source=Toshiba-PC;Initial Catalog=data_karyawan;Integrated Security=true;User Id=sa;password=bombom123;";

    protected void Page_Load(object sender, EventArgs e)
    {
        DropForumTitle();
        if (!IsPostBack)
        {
            clear();
            Gridfill();
        }
    }
        protected void btnClear_Click(object sender, EventArgs e)
    {

    }

          protected void DropForumTitle()
    {
        if (!Page.IsPostBack)
        {
          
                     
            string selectSQL = "select DISTINCT Department_ID,Department_ID from Department_Data";
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(selectSQL, con);
            SqlDataReader reader;
            try
            {
               
                ListItem newItem = new ListItem();
                newItem.Text = "Select";
                newItem.Value = "0";
                DropDownList3.Items.Add(newItem);
                con.Open();
                reader = cmd.ExecuteReader();



                while (reader.Read())
                {
                    ListItem newItem1 = new ListItem();
                    newItem1.Text = reader["Department_ID"].ToString();
                    newItem1.Value = reader["Department_ID"].ToString();
                    DropDownList3.Items.Add(newItem1);



                }
                reader.Close();
                reader.Dispose();
                con.Close();
                con.Dispose();
                cmd.Dispose();


            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            //////////////////

        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
         try
        {
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();
                SqlCommand sqlCmd = new SqlCommand("insert_karyawan", SqlCon);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("KID", TxtKID.Text.Trim());
                sqlCmd.Parameters.AddWithValue("DepartmentID", DropDownList3.Text.Trim());
                sqlCmd.Parameters.AddWithValue("Nama", TxtNama.Text.Trim());
                sqlCmd.Parameters.AddWithValue("Alamat", TxtAlamat.Text.Trim());
                sqlCmd.Parameters.AddWithValue("TTL", TxtTTL.Text.Trim());
                sqlCmd.Parameters.AddWithValue("NoTelpon", TxtNoTelp.Text.Trim());
                sqlCmd.Parameters.AddWithValue("Sts", DropDownList2.Text.Trim());
                sqlCmd.Parameters.AddWithValue("duit", Convert.ToInt32(TxtPenghasilan.Text.Trim()));
                sqlCmd.ExecuteNonQuery();
                Gridfill();
                clear();
                lblSuccessMessage.Text = "Submitted Successfully";
            }
        }
         catch (Exception ex)
         {
             lblErrorMessage.Text = ex.Message;
         }
    }
    void clear()
    {
        TxtKID.Text=TxtAlamat.Text=TxtNama.Text=TxtNoTelp.Text=TxtPenghasilan.Text=TxtTTL.Text=DropDownList3.Text=DropDownList2.Text="";
         btnSave.Text = "Save";
        btnDelete.Enabled = false;
        lblErrorMessage.Text = lblSuccessMessage.Text = "";
    }
    void Gridfill()
    {
        using (SqlConnection SqlCon = new SqlConnection(connectionString))
        {
            SqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("KaryawanViewAll", SqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
            gvProduct.DataSource = dtbl;
            gvProduct.DataBind();
        }
    }
    }
