﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class general : System.Web.UI.Page
{
    string connectionString = @"Data Source=Toshiba-PC;Initial Catalog=data_karyawan;Integrated Security=true;User Id=sa;password=bombom123;";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            clear();
            Gridfill();
        }
             
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();
                SqlCommand sqlCmd = new SqlCommand("DepartmentCreateOrUpdate", SqlCon);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("DepartmentID", txtDepartment.Text.Trim());
                sqlCmd.Parameters.AddWithValue("BagianGawe", txtBidang.Text.Trim());
                sqlCmd.Parameters.AddWithValue("Posisi", txtPosisi.Text.Trim());
                sqlCmd.ExecuteNonQuery();
                Gridfill();
                clear();
                lblSuccessMessage.Text = "Submitted Successfully";

            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.Message;
        }
    }
    void clear()
    {

        txtDepartment.Text = txtBidang.Text = txtPosisi.Text = "";
        btnSave.Text = "Save";
        btnDelete.Enabled = false;
        lblErrorMessage.Text = lblSuccessMessage.Text = "";
    }
    void Gridfill()
    {
        using (SqlConnection SqlCon = new SqlConnection(connectionString))
        {
            SqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("DepartmentViewAll", SqlCon);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
            gvProduct.DataSource = dtbl;
            gvProduct.DataBind();
        }
    }
    protected void lnkSelect_OnClick(object sender, EventArgs e)
    {
        var Department_ID = Convert.ToString((sender as LinkButton).CommandArgument);
        using (SqlConnection SqlCon = new SqlConnection(connectionString))
        {
            SqlCon.Open();
            SqlDataAdapter sqlDa = new SqlDataAdapter("DepartmentViewByID", SqlCon);
            sqlDa.SelectCommand.Parameters.AddWithValue("DepartmentID", Department_ID);
            sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dtbl = new DataTable();
            sqlDa.Fill(dtbl);
            txtDepartment.Text = dtbl.Rows[0][0].ToString();
            txtBidang.Text = dtbl.Rows[0][1].ToString();
            txtPosisi.Text = dtbl.Rows[0][2].ToString();
            btnDelete.Enabled = true;
            btnSave.Enabled = false;
        }
    }



    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();
                SqlCommand sqlCmd = new SqlCommand("DepartmentUpdateae", SqlCon);
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.AddWithValue("DepartmentID", txtDepartment.Text.Trim());
                sqlCmd.Parameters.AddWithValue("BagianGawe", txtBidang.Text.Trim());
                sqlCmd.Parameters.AddWithValue("Posisi", txtPosisi.Text.Trim());
                sqlCmd.ExecuteNonQuery();
                Gridfill();
                clear();
                lblSuccessMessage.Text = "Update Successfully";

            }
        }
        catch (Exception ex)
        {
            lblErrorMessage.Text = ex.Message;
        }

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        using (SqlConnection SqlCon = new SqlConnection(connectionString))
        {
            SqlCon.Open();
            SqlCommand sqlCmd = new SqlCommand("DeleteDepartmentByID", SqlCon);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("DepID", txtDepartment.Text.Trim());
            sqlCmd.ExecuteNonQuery();
            Gridfill();
            clear();
            lblSuccessMessage.Text = "Delete Successfully";
            btnSave.Enabled = true;

        }
    }
    public void search()
    {
        using (SqlConnection SqlCon = new SqlConnection(connectionString))
        {
            {

                DataView DV = new DataView();

                DV.RowFilter = string.Format("Department_ID LIKE '%{0}%'", TextBox1.Text);

                gvProduct.DataSource = DV;
                SqlCon.Close();
            }
        }
    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection Conn = new SqlConnection(connectionString);
     DataTable dt = new DataTable();
     SqlDataAdapter SDA = new SqlDataAdapter("Select * from Department_Data where Department_ID like'" + TextBox1.Text + "'", Conn);
     SDA.Fill(dt);
          gvProduct.DataSource = dt;
          gvProduct.DataBind();
    }
}