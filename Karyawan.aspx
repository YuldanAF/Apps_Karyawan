﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Karyawan.aspx.cs" Inherits="Karyawan" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     <div>
       <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
       <asp:Button ID="Button1" runat="server"  Text="Button" />
    <table>
    <tr>
        <td><asp:Label ID="Label1" Text="Karyawan_ID" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="TxtKID" class="textfield" runat="server" />
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label2"  Text="Department_ID" runat="server"/></td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownList3" runat="server" selectedvalue='<%# Bind("Department_ID") %>'>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label3"  Text="Nama_Karyawan" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="TxtNama" class="textfield" runat="server" />
        </td>
    </tr>
        <tr>
        <td><asp:Label ID="Label4" Text="Alamat_Karyawan" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="TxtAlamat" class="textfield" runat="server" />
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label5"  Text="TTL" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="TxtTTL" class="textfield" runat="server" TextMode="Date" />
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label6"  Text="No_Telp" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="TxtNoTelp" class="textfield" runat="server" />
        </td>
    </tr>
        <tr>
        <td><asp:Label ID="Label7" Text="Status" runat="server"/></td>
        <td colspan="2">
            <asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem>Aktif</asp:ListItem>
                <asp:ListItem>Baru</asp:ListItem>
                <asp:ListItem>Resign</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label8"  Text="Penghasilan" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="TxtPenghasilan" class="textfield" runat="server" />
        </td>
    </tr>
    <tr>
    <td></td>
    <td colspan="2">
    <asp:Button Text="Save" ID="btnSave" class="buttonform" runat="server" OnClick="btnSave_Click"   />
    <asp:Button Text="Delete" ID="btnDelete" class="buttonform" runat="server"   />
    <asp:Button Text="Clear" ID="btnClear" class="buttonform" runat="server" OnClick="btnClear_Click"     />
        <asp:Button Text="Update" ID="btnUpdate" class="buttonform" runat="server"     />
    </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td colspan="2">
    <asp:Label Text="" ID="lblSuccessMessage" runat="server" ForeColor="Green" />

    </td>
    </tr>
    <tr>
    <td></td>
    <td colspan="2">
    <asp:Label Text="" ID="lblErrorMessage" runat="server" ForeColor="Red" />

    </td>
    </tr>
    </table>
    <br/ . />
    <asp:GridView ID="gvProduct" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundField DataField="Karyawan_ID" HeaderText="Karyawan_ID" />
    <asp:BoundField DataField="Department_ID"  HeaderText="Department_ID" />
    <asp:BoundField DataField="Nama_Karyawan"  HeaderText="Nama_Karyawan" />
    <asp:BoundField DataField="Alamat_Karyawan" HeaderText="Alamat_Karyawan" />
    <asp:BoundField DataField="TTL"  HeaderText="TTL" />
    <asp:BoundField DataField="No_Telp"  HeaderText="No_Telp" />
    <asp:BoundField DataField="Status" HeaderText="Status" />
    <asp:BoundField DataField="Penghasilan"  HeaderText="Penghasilan" />
    
         <asp:TemplateField>
        <ItemTemplate>
        <asp:LinkButton Text="Select" ID="lnkSelect"  runat="server"  />        </ItemTemplate>
    </asp:TemplateField>
    </Columns>
    
    </asp:GridView>
      <asp:Parameter Name="Department_ID" ConvertEmptyStringToNull="false" Type="Char" />
    </div>
    </form>
</body>
</html>
