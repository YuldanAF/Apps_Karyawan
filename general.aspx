﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="general.aspx.cs" Inherits="general" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
   <div>
       <asp:TextBox ID="TextBox1" runat="server" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
       <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
    <table>
    <tr>
        <td><asp:Label ID="Label1" Text="Department_ID" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="txtDepartment" class="textfield" runat="server" />
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label2"  Text="Bidang_Keahlian" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="txtBidang" class="textfield" runat="server" />
        </td>
    </tr>
    <tr>
        <td><asp:Label ID="Label3"  Text="Posisi_Tim" runat="server"/></td>
        <td colspan="2">
            <asp:TextBox ID="txtPosisi" class="textfield" runat="server" />
        </td>
    </tr>
    <tr>
    <td></td>
    <td colspan="2">
    <asp:Button Text="Save" ID="btnSave" class="buttonform" runat="server"  onclick="btnSave_Click" />
    <asp:Button Text="Delete" ID="btnDelete" class="buttonform" runat="server" OnClick="btnDelete_Click"  />
    <asp:Button Text="Clear" ID="btnClear" class="buttonform" runat="server" OnClick="btnClear_Click"    />
        <asp:Button Text="Update" ID="btnUpdate" class="buttonform" runat="server" OnClick="btnUpdate_Click"    />
    </td>
    </tr>
    <tr>
    <td></td>
    <td colspan="2">
    <asp:Label Text="" ID="lblSuccessMessage" runat="server" ForeColor="Green" />

    </td>
    </tr>
    <tr>
    <td></td>
    <td colspan="2">
    <asp:Label Text="" ID="lblErrorMessage" runat="server" ForeColor="Red" />

    </td>
    </tr>
    </table>
    <br/ . />
    <asp:GridView ID="gvProduct" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundField DataField="Department_ID" HeaderText="Department_ID" />
    <asp:BoundField DataField="Bidang_Keahlian"  HeaderText="Bidang_Keahlian" />
    <asp:BoundField DataField="Posisi_Tim"  HeaderText="Posisi_Tim" />
     <asp:TemplateField>
        <ItemTemplate>
        <asp:LinkButton Text="Select" ID="lnkSelect" CommandArgument='<%# Eval("Department_ID") %>' runat="server" OnClick="lnkSelect_OnClick" />        </ItemTemplate>
    </asp:TemplateField>
    </Columns>
    
    </asp:GridView>
    </div>
    </form>
</body>
</html>
